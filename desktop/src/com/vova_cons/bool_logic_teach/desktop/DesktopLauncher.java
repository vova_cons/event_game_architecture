package com.vova_cons.bool_logic_teach.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.vova_cons.stargate_game.core.Core;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Search & Sell";
		config.width = 1280;
		config.height = 720;
		new LwjglApplication(Core.getInstance(), config);
	}
}
