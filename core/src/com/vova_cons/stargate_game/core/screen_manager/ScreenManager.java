package com.vova_cons.stargate_game.core.screen_manager;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.Disposable;
import com.vova_cons.stargate_game.core.Core;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.game_screen.GameScreen;
import com.vova_cons.stargate_game.screens.menu_screen.MenuScreen;
import com.vova_cons.stargate_game.screens.new_game_screen.NewGameScreen;
import com.vova_cons.stargate_game.services.assets_service.AssetsService;

import java.util.HashMap;

/**
 * Created by anbu on 25.04.18.
 */
public class ScreenManager implements Disposable {
    private HashMap<Class<? extends BaseScreen>, BaseScreen> screenMap = new HashMap<Class<? extends BaseScreen>, BaseScreen>();
    private Class<? extends BaseScreen> currentScreen = null;
    private ScreenManagerListener listener;

    public ScreenManager(ScreenManagerListener listener) {
        this.listener = listener;
        register(new MenuScreen());
        register(new GameScreen());
        register(new NewGameScreen());
    }


    private void register(BaseScreen screen){
        screenMap.put(screen.getClass(), screen);
    }

    public void setScreen(Class<? extends BaseScreen> screenClass){
        if (screenMap.containsKey(screenClass)) {
            this.currentScreen = screenClass;
            AssetsService assets = Core.getInstance().getServicesManager().getService(AssetsService.class);
            assets.load(screenClass);
            listener.setScreen(screenMap.get(screenClass));
        }
    }

    @Override
    public void dispose () {
        for(Screen screen : screenMap.values()) {
            screen.dispose();
        }
        screenMap.clear();
    }
}
