package com.vova_cons.stargate_game.core.screen_manager;

import com.badlogic.gdx.Screen;

/**
 * Created by anbu on 25.04.18.
 */
public interface ScreenManagerListener {
    void setScreen(Screen screen);
}
