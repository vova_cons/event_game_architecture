package com.vova_cons.stargate_game.core.services_manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.vova_cons.stargate_game.services.Service;
import com.vova_cons.stargate_game.services.assets_service.AssetsService;
import com.vova_cons.stargate_game.services.assets_service.AssetsXmlParser;
import com.vova_cons.stargate_game.services.font_service.FontService;
import com.vova_cons.stargate_game.services.game_progress_service.GameProgressService;
import com.vova_cons.stargate_game.services.log_service.LogService;

import java.util.HashMap;

/**
 * Created by anbu on 25.04.18.
 */
public class ServicesManager implements Service {
    private HashMap<Class<? extends Service>, Service> serviceHashMap = new HashMap<Class<? extends Service>, Service>();

    public ServicesManager() {
        register(new FontService());
        register(new LogService());
        FileHandle file = Gdx.files.internal("assets.xml");
        if (file.exists()) {
            register(AssetsXmlParser.parse(file));
        }
        register(new GameProgressService());
    }

    private void register(Service service){
        serviceHashMap.put(service.getClass(), service);
    }

    public <T> T getService(Class<? extends T> type){
        if (serviceHashMap.containsKey(type)) {
            Service service = serviceHashMap.get(type);
            try {
                return (T)service;
            } catch (Exception e){
                return null;
            }
        }
        return null;
    }

    //region interface
    @Override
    public void onStart() {
        for(Service service : serviceHashMap.values()){
            service.onStart();
        }
    }

    @Override
    public void update(float delta) {
        for(Service service : serviceHashMap.values()){
            service.update(delta);
        }
    }

    @Override
    public void onFinish() {
        for(Service service : serviceHashMap.values()){
            service.onFinish();
        }
    }
    //endregion
}
