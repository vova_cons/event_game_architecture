package com.vova_cons.stargate_game.core;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.vova_cons.stargate_game.core.screen_manager.ScreenManager;
import com.vova_cons.stargate_game.core.screen_manager.ScreenManagerListener;
import com.vova_cons.stargate_game.core.services_manager.ServicesManager;
import com.vova_cons.stargate_game.screens.menu_screen.MenuScreen;
import com.vova_cons.stargate_game.screens.new_game_screen.NewGameScreen;
import com.vova_cons.stargate_game.services.assets_service.AssetsService;

public class Core extends Game implements ScreenManagerListener {
    private static Core instance = null;

    private ScreenManager screenManager;
    private ServicesManager servicesManager;

    private Core() {}

    public void init() {
        this.screenManager = new ScreenManager(this);
        this.servicesManager = new ServicesManager();
    }

    public static Core getInstance() {
        if (instance == null) {
            instance = new Core();
        }
        return instance;
    }

	@Override
	public void create () {
        init();
        servicesManager.onStart();
		screenManager.setScreen(NewGameScreen.class);
	}

    @Override
    public void dispose () {
        screenManager.dispose();
        servicesManager.onFinish();
    }

    @Override
    public void render() {
        float delta = Gdx.graphics.getDeltaTime();
        servicesManager.update(delta);
        if (screen != null) {
            screen.render(delta);
        }
    }

    //region interface
    public ScreenManager getScreenManager() {
        return screenManager;
    }

    public ServicesManager getServicesManager() {
        return servicesManager;
    }
    //endregion
}
