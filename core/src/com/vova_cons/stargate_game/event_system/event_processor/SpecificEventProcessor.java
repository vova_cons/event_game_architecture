package com.vova_cons.stargate_game.event_system.event_processor;

import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.World;

/**
 * Created by anbu on 12.06.18.
 */
public interface SpecificEventProcessor<T, K> {
    void update(float delta, World<K> world, Event<T> event);
}
