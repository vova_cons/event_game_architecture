package com.vova_cons.stargate_game.event_system.event_processor;

import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.EventQueue;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.clock_machine.ClockMachineListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anbu on 12.06.18.
 */
public class EventProcessor<T, K> implements ClockMachineListener {
    private Map<T, SpecificEventProcessor<T, K>> specificEventProcessorMap = new HashMap<T, SpecificEventProcessor<T, K>>();
    private EventQueue<T> eventQueue;
    private World<K> world;

    public void setEventQueue(EventQueue<T> eventQueue) {
        this.eventQueue = eventQueue;
    }

    public void setWorld(World<K> world) {
        this.world = world;
    }

    public void addSpecificEventProcessor(T type, SpecificEventProcessor<T, K> specificEventProcessor){
        specificEventProcessorMap.put(type, specificEventProcessor);
    }

    //region interface
    @Override
    public void update(float delta){
        Event<T> event = eventQueue.next();
        while (event != null) {
            SpecificEventProcessor<T, K> specificEventProcessor = specificEventProcessorMap.get(event.getType());
            specificEventProcessor.update(delta, world, event);
            event = eventQueue.next();
        }
    }
    //endregion
}
