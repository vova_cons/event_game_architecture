package com.vova_cons.stargate_game.event_system;

/**
 * Created by anbu on 12.06.18.
 */
public interface EventHandler<T> {
    void handle(Event<T> event);
}
