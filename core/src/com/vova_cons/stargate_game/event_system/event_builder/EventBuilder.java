package com.vova_cons.stargate_game.event_system.event_builder;

import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.EventHandler;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.clock_machine.ClockMachineListener;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by anbu on 12.06.18.
 */
public class EventBuilder<T, K> implements ClockMachineListener {
    private EventHandler<T> eventHandler;
    private World<K> world;
    private List<SpecificEventBuilder<T, K>> specificEventBuilders = new LinkedList<SpecificEventBuilder<T, K>>();

    public void setEventHandler(EventHandler<T> eventHandler) {
        this.eventHandler = eventHandler;
    }

    public void setWorld(World<K> world) {
        this.world = world;
    }

    public void addSpecificEventBuilder(SpecificEventBuilder<T, K> specificEventBuilder){
        specificEventBuilders.add(specificEventBuilder);
    }

    //region interface
    @Override
    public void update(float delta){
        for(SpecificEventBuilder<T, K> specificEventBuilder : specificEventBuilders){
            specificEventBuilder.update(delta, world, eventHandler);
        }
    }
    //endregion
}
