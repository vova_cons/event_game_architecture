package com.vova_cons.stargate_game.event_system;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by anbu on 12.06.18.
 */
public class World <T> extends Group {
    private Map<String, Actor> objectsMap = new HashMap<String, Actor>();
    private Map<T, List<Actor>> objectsTypesMap = new HashMap<T, List<Actor>>();

    //region logic
    public Actor getActor(String id){
        return objectsMap.get(id);
    }

    public void remove(String id){
        Actor actor = objectsMap.get(id);
        if (actor != null) {
            objectsMap.remove(id);
            for(List<Actor> actors : objectsTypesMap.values()){
                actors.remove(actor);
            }
        } else {
            System.err.println("not found for remove: " + id);
        }
    }

    public List<Actor> getTypeActors(T type) {
        return objectsTypesMap.get(type);
    }

    public void registerActor(String id, T type, Actor actor){
        this.objectsMap.put(id, actor);
        if (!this.objectsTypesMap.containsKey(type)){
            objectsTypesMap.put(type, new LinkedList<Actor>());
        }
        objectsTypesMap.get(type).add(actor);
    }
    //endregion
}
