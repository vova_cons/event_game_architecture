package com.vova_cons.stargate_game.event_system.clock_machine;

/**
 * Created by anbu on 12.06.18.
 */
public interface ClockMachineListener {
    void update(float delta);
}
