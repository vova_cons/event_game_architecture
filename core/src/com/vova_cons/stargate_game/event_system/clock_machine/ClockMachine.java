package com.vova_cons.stargate_game.event_system.clock_machine;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by anbu on 12.06.18.
 */
public class ClockMachine {
    private List<ClockMachineListener> listeners = new LinkedList<ClockMachineListener>();

    public void addListener(ClockMachineListener listener){
        listeners.add(listener);
    }

    public void removeListener(ClockMachineListener listener){
        listeners.remove(listener);
    }

    public void update(float delta){
        for(ClockMachineListener listener : listeners){
            listener.update(delta);
        }
    }
}
