package com.vova_cons.stargate_game.event_system;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by anbu on 12.06.18.
 */
public class EventBuffer<T> implements EventHandler<T>, EventQueue<T> {
    private Queue<Event<T>> eventQueue = new LinkedList<Event<T>>();

    @Override
    public void handle(Event<T> event) {
        eventQueue.add(event);
    }

    @Override
    public Event<T> next() {
        return eventQueue.poll();
    }
}
