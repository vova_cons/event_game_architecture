package com.vova_cons.stargate_game.event_system;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anbu on 12.06.18.
 */
public class Event<T> {
    private T type;
    private Map<String, Object> args = new HashMap<String, Object>();

    public Event(T type) {
        this.type = type;
    }

    public void setArg(String key, Object value){
        args.put(key, value);
    }

    public T getType() {
        return type;
    }

    public <K> K getArg(String id) {
        if (!args.containsKey(id)){
            return null;
        }
        try {
            Object obj = args.get(id);
            return (K) obj;
        } catch (Exception e){
            return null;
        }
    }
}
