package com.vova_cons.stargate_game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Created by anbu on 25.04.18.
 *
 */
public abstract class BaseScreen implements Screen {
    public static final float WIDTH = 1280f;
    public  static float REAL_WIDTH = 1280f;
    public static final float HEIGHT = 720f;
    private Stage stage;
    private FitViewport viewport;
    private Group gameGroup;
    private InputMultiplexer inputMultiplexer;


    //region implementation
    @Override
    public void show() {
        if (stage == null) {
            float screenRatio = ((float) Gdx.graphics.getWidth()) / Gdx.graphics.getHeight();
            REAL_WIDTH = screenRatio * HEIGHT;
            viewport = new FitViewport(REAL_WIDTH, HEIGHT);
            stage = new Stage(viewport);
            inputMultiplexer = new InputMultiplexer(stage);
            gameGroup = new Group();
            float xFix = (REAL_WIDTH - WIDTH) / 2f;
            gameGroup.setX(xFix);
            stage.addActor(gameGroup);
            gameGroup.setSize(WIDTH, HEIGHT);
        }
        Gdx.input.setInputProcessor(inputMultiplexer);
        this.onStart();
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        if (viewport != null) {
            viewport.update(width, height);
        }
    }

    @Override
    public void pause() {
        if (stage != null) {
            stage.draw();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        if (stage != null) {
            stage.dispose();
            stage = null;
        }
        this.onFinish();
    }
    //endregion


    //region interface
    protected abstract void onStart();

    protected abstract void onFinish();

    protected abstract void update(float delta);
    //endregion

    public void addActor(Actor actor){
        gameGroup.addActor(actor);
    }
}
