package com.vova_cons.stargate_game.screens.menu_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.vova_cons.stargate_game.core.Core;
import com.vova_cons.stargate_game.core.screen_manager.ScreenManager;
import com.vova_cons.stargate_game.core.services_manager.ServicesManager;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.game_screen.GameScreen;
import com.vova_cons.stargate_game.services.font_service.FontService;
import com.vova_cons.stargate_game.services.log_service.LogService;

/**
 * Created by anbu on 25.04.18.
 */
public class MenuScreen extends BaseScreen {

    @Override
    protected void onStart() {
        FontService fontService = (FontService) Core.getInstance().getServicesManager().getService(FontService.class);
        Label.LabelStyle labelStyle = new Label.LabelStyle(fontService.getFont(FontService.Size.H1), Color.WHITE);
        Label label = new Label("Game Screen 123 ∧∨", labelStyle);
        label.setAlignment(Align.center);
        label.setY(200);
        label.setSize(500, 50);
        this.addActor(label);
        Label.LabelStyle labelStyle2 = new Label.LabelStyle(fontService.getFont(FontService.Size.H2), Color.WHITE);
        Label label2 = new Label("Game Screen 123", labelStyle2);
        label2.setAlignment(Align.center);
        label2.setSize(500, 50);
        label2.setY(150);
        this.addActor(label2);
        Label.LabelStyle labelStyle3 = new Label.LabelStyle(fontService.getFont(FontService.Size.H3), Color.WHITE);
        Label label3 = new Label("Game Screen 123", labelStyle3);
        label3.setAlignment(Align.center);
        label3.setSize(500, 50);
        label3.setY(100);
        this.addActor(label3);
        Label.LabelStyle labelStyle4 = new Label.LabelStyle(fontService.getFont(FontService.Size.P), Color.WHITE);
        Label label4 = new Label("Game Screen 123", labelStyle4);
        label4.setAlignment(Align.center);
        label4.setSize(500, 50);
        label4.setY(50);
        this.addActor(label4);
        Label.LabelStyle labelStyle5 = new Label.LabelStyle(fontService.getFont(FontService.Size.Small), Color.WHITE);
        Label label5 = new Label("Game Screen 123", labelStyle5);
        label5.setAlignment(Align.center);
        label5.setSize(500, 50);
        label5.setY(0);
        this.addActor(label5);

        Texture texture = new Texture(Gdx.files.internal("badlogic.jpg"));
        Image image = new Image(texture);
        image.setPosition(500 + image.getWidth()/2, 100, Align.center);
        image.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                start("");
            }
        });
        this.addActor(image);
        ServicesManager serviceManager = Core.getInstance().getServicesManager();
        LogService logService = (LogService) serviceManager.getService(LogService.class);
        logService.log("changed to menu screen");
    }

    @Override
    protected void onFinish() {

    }

    @Override
    protected void update(float delta) {
        Gdx.input.setCatchBackKey(true);
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK) ||Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            Gdx.app.exit();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_0)){
            start("test");
        }
    }

    private void start(String level) {
        ScreenManager screenManager = Core.getInstance().getScreenManager();
        screenManager.setScreen(GameScreen.class);
    }
}
