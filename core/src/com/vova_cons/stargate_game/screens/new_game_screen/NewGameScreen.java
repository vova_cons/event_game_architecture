package com.vova_cons.stargate_game.screens.new_game_screen;

import com.vova_cons.stargate_game.event_system.EventBuffer;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.clock_machine.ClockMachine;
import com.vova_cons.stargate_game.event_system.event_builder.EventBuilder;
import com.vova_cons.stargate_game.event_system.event_processor.EventProcessor;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.new_game_screen.event_builder.*;
import com.vova_cons.stargate_game.screens.new_game_screen.event_processor.*;

/**
 * Created by anbu on 12.06.18.
 */
public class NewGameScreen extends BaseScreen {
    private ClockMachine clockMachine;

    @Override
    protected void onStart() {
        World<GameActorType> world = new WorldBuilder().build();
        EventBuilder<GameEventType, GameActorType> eventBuilder = new EventBuilder<GameEventType, GameActorType>();
        eventBuilder.setWorld(world);
        EventBuffer<GameEventType> eventBuffer = new EventBuffer<GameEventType>();
        eventBuilder.setEventHandler(eventBuffer);
        EventProcessor<GameEventType, GameActorType> eventProcessor = new EventProcessor<GameEventType, GameActorType>();
        eventProcessor.setWorld(world);
        eventProcessor.setEventQueue(eventBuffer);
        clockMachine = new ClockMachine();
        clockMachine.addListener(eventBuilder);
        clockMachine.addListener(eventProcessor);

        eventBuilder.addSpecificEventBuilder(new PlayerMovingEventBuilder());
        eventBuilder.addSpecificEventBuilder(new PlayerSeeEventBuilder());
        eventBuilder.addSpecificEventBuilder(new CameraFollowPlayerEventBuilder());
        eventBuilder.addSpecificEventBuilder(new PlayerFireEventBuilder());
        eventBuilder.addSpecificEventBuilder(new BulletUpdateEventBuilder());

        eventProcessor.addSpecificEventProcessor(GameEventType.SetPosition, new ActorSetPositionProcessor());
        eventProcessor.addSpecificEventProcessor(GameEventType.MoveTo, new ActorMovingProcessor());
        eventProcessor.addSpecificEventProcessor(GameEventType.SeeTo, new ActorSeeProcessor());
        eventProcessor.addSpecificEventProcessor(GameEventType.CameraPositionSet, new CameraFollowProcessor());
        eventProcessor.addSpecificEventProcessor(GameEventType.BulletRemove, new BulletRemoveProcessor());
        eventProcessor.addSpecificEventProcessor(GameEventType.BulletAdd, new BulletAddProcessor());

        this.addActor(world);
    }

    @Override
    protected void onFinish() {
        clockMachine = null;
    }

    @Override
    protected void update(float delta) {
        clockMachine.update(delta);
    }
}
