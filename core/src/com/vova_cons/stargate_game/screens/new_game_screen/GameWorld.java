package com.vova_cons.stargate_game.screens.new_game_screen;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.vova_cons.stargate_game.event_system.World;

/**
 * Created by anbu on 12.06.18.
 */
public class GameWorld extends World<GameActorType> {
    private Group backgroundLayer;
    private Group middlegroundLayer;
    private Group hovergroundLayer;

    public GameWorld() {
        createLayers();
    }

    private void createLayers() {
        backgroundLayer = new Group();
        this.addActor(backgroundLayer);
        middlegroundLayer = new Group();
        this.addActor(middlegroundLayer);
        hovergroundLayer = new Group();
        this.addActor(hovergroundLayer);
    }

    public void addBackgroundActor(String id, GameActorType type, Actor actor){
        backgroundLayer.addActor(actor);
        registerActor(id, type, actor);
    }

    public void addMiddlegroundActor(String id, GameActorType type, Actor actor){
        middlegroundLayer.addActor(actor);
        registerActor(id, type, actor);
    }

    public void addHovergroundActor(String id, GameActorType type, Actor actor){
        hovergroundLayer.addActor(actor);
        registerActor(id, type, actor);
    }

    @Override
    public void remove(String id) {
        Actor actor = super.getActor(id);
        super.remove(id);
        if (actor != null) {
            backgroundLayer.removeActor(actor);
            middlegroundLayer.removeActor(actor);
            hovergroundLayer.removeActor(actor);
        }
    }
}
