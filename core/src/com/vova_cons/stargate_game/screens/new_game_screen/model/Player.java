package com.vova_cons.stargate_game.screens.new_game_screen.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

/**
 * Created by anbu on 16.06.18.
 */
public class Player extends Unit {
    private Weapon weapon;

    public Player(String id) {
        super(id);
        this.weapon = WeaponBuilder.create(WeaponBuilder.Type.Ak74);
        Image player = new Image(new Texture("player/rifle.png"));
        player.setSize(100, 100);
        this.setSize(player.getWidth(), player.getHeight());
        this.addActor(player);
        this.setPosition(0, 0, Align.center);
        this.setOrigin(player.getWidth()*0.40f, player.getHeight()*0.25f);
    }

    public Weapon getWeapon() {
        return weapon;
    }
}
