package com.vova_cons.stargate_game.screens.new_game_screen.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by anbu on 12.06.18.
 */
public class Bullet extends Image {
    private String id;
    private float speed;
    private Vector2 beginPosition;
    private Vector2 endPosition;
    private float progress = 0;

    public Bullet(String id, Texture texture) {
        super(texture);
        this.id = id;
        this.setSize(14, 7);
    }

    //region setters
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public void setBeginPosition(Vector2 beginPosition) {
        this.beginPosition = beginPosition;
    }

    public void setEndPosition(Vector2 endPosition) {
        this.endPosition = endPosition;
    }
    //endregion

    //region getters
    public String getId() {
        return id;
    }

    public float getProgress() {
        return progress;
    }

    public Vector2 getBeginPosition() {
        return beginPosition;
    }

    public Vector2 getEndPosition() {
        return endPosition;
    }

    public float getSpeed() {
        return speed;
    }
    //endregion
}
