package com.vova_cons.stargate_game.screens.new_game_screen.model;

import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * Created by anbu on 16.06.18.
 */
public class Unit extends Group {
    private String id;

    public Unit(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
