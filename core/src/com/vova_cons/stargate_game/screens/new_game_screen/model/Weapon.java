package com.vova_cons.stargate_game.screens.new_game_screen.model;

/**
 * Created by anbu on 16.06.18.
 */
public class Weapon {
    private String id;
    private int bulletsCapacity;
    private int bulletsCount;
    private float reloadTime;
    private float fireSpeed;
    private float accuracyRadius;

    public String getId() {
        return id;
    }

    public int getBulletsCapacity() {
        return bulletsCapacity;
    }

    public int getBulletsCount() {
        return bulletsCount;
    }

    public float getReloadTime() {
        return reloadTime;
    }

    public float getFireSpeed() {
        return fireSpeed;
    }

    public float getAccuracyRadius() {
        return accuracyRadius;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFireSpeed(float fireSpeed) {
        this.fireSpeed = fireSpeed;
    }

    public void setBulletsCapacity(int bulletsCapacity) {
        this.bulletsCapacity = bulletsCapacity;
    }

    public void setBulletsCount(int bulletsCount) {
        this.bulletsCount = bulletsCount;
    }

    public void setReloadTime(float reloadTime) {
        this.reloadTime = reloadTime;
    }

    public void setAccuracyRadius(float accuracyRadius) {
        this.accuracyRadius = accuracyRadius;
    }
}
