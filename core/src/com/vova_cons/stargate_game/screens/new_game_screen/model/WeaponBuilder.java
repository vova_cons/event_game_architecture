package com.vova_cons.stargate_game.screens.new_game_screen.model;

/**
 * Created by anbu on 16.06.18.
 */
public class WeaponBuilder {
    enum Type {
        Ak74
    }

    public static Weapon create(Type type) {
        switch(type){
            case Ak74:
                return createAk74();
        }
        return null;
    }

    private static Weapon createAk74() {
        Weapon weapon = new Weapon();
        weapon.setId(Type.Ak74.name());
        weapon.setBulletsCapacity(30);
        weapon.setBulletsCount(30);
        weapon.setReloadTime(3.5f);
        weapon.setFireSpeed(60f/600f); // 600 bullets per minute
        weapon.setAccuracyRadius(100f);
        return weapon;
    }
}
