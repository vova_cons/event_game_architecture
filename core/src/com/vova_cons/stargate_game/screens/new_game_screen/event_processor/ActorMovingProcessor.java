package com.vova_cons.stargate_game.screens.new_game_screen.event_processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_processor.SpecificEventProcessor;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;

/**
 * Created by anbu on 12.06.18.
 */
public class ActorMovingProcessor implements SpecificEventProcessor<GameEventType, GameActorType> {
    public static final String ID = "id";
    public static final String VECTOR = "vector";

    @Override
    public void update(float delta, World<GameActorType> world, Event<GameEventType> event) {
        String id = event.getArg(ID);
        Actor player = world.getActor(id);
        Vector2 vector = event.getArg(VECTOR);
        if (player == null) {
            Gdx.app.error("ActorMovingProcessor", "actor are null id=" + id);
            return;
        }
        if (vector == null) {
            Gdx.app.error("ActorMovingProcessor", "vector are null id=" + id);
            return;
        }
        player.addAction(Actions.moveBy(vector.x, vector.y));
    }
}
