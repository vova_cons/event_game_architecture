package com.vova_cons.stargate_game.screens.new_game_screen.event_processor;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_processor.SpecificEventProcessor;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;

/**
 * Created by anbu on 13.06.18.
 */
public class BulletRemoveProcessor implements SpecificEventProcessor<GameEventType, GameActorType> {
    @Override
    public void update(float delta, World<GameActorType> world, Event<GameEventType> event) {
        String id = event.getArg("id");
        world.remove(id);
    }
}
