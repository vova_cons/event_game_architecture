package com.vova_cons.stargate_game.screens.new_game_screen.event_processor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_processor.SpecificEventProcessor;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameWorld;
import com.vova_cons.stargate_game.screens.new_game_screen.event_builder.PlayerFireEventBuilder;
import com.vova_cons.stargate_game.screens.new_game_screen.model.Bullet;

/**
 * Created by anbu on 13.06.18.
 */
public class BulletAddProcessor implements SpecificEventProcessor<GameEventType, GameActorType> {
    private Texture texture = new Texture("player/bullet.png");

    @Override
    public void update(float delta, World<GameActorType> world, Event<GameEventType> event) {
        String id = event.getArg(PlayerFireEventBuilder.ID);
        Vector2 start = event.getArg(PlayerFireEventBuilder.START);
        Vector2 finish = event.getArg(PlayerFireEventBuilder.FINISH);
        float angle = event.getArg(PlayerFireEventBuilder.ANGLE);
        float speed = event.getArg(PlayerFireEventBuilder.SPEED);
        Bullet bullet = new Bullet(id, texture);
        bullet.setBeginPosition(start);
        bullet.setEndPosition(finish);
        bullet.setSpeed(speed);
        bullet.setProgress(0f);
        bullet.setPosition(start.x, start.y, Align.center);
        bullet.setOrigin(Align.center);
        bullet.setRotation(angle);
        GameWorld gworld = (GameWorld) world;
        gworld.addHovergroundActor(id, GameActorType.Bullet, bullet);
    }
}
