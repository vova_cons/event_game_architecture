package com.vova_cons.stargate_game.screens.new_game_screen.event_processor;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_processor.SpecificEventProcessor;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;

/**
 * Created by anbu on 12.06.18.
 */
public class CameraFollowProcessor implements SpecificEventProcessor<GameEventType, GameActorType> {
    @Override
    public void update(float delta, World<GameActorType> world, Event<GameEventType> event) {
        Vector2 pos = event.getArg("position");
        float x = -pos.x + BaseScreen.WIDTH/2f;
        float y = -pos.y + BaseScreen.HEIGHT/2f;
        world.setPosition(x, y);
    }
}
