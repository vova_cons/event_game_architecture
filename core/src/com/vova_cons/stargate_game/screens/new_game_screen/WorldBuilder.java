package com.vova_cons.stargate_game.screens.new_game_screen;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.screens.new_game_screen.model.Player;

import java.util.Random;

/**
 * Created by anbu on 12.06.18.
 */
public class WorldBuilder {
    private GameWorld world;
    private Random random = new Random(System.currentTimeMillis());

    public World<GameActorType> build(){
        world = new GameWorld();
        createPlayer();
        createZombies();
        return world;
    }

    private void createPlayer() {
        Player player = new Player("player");
        world.addMiddlegroundActor(player.getId(), GameActorType.Player, player);
        Actor missle = new Actor();
        world.addMiddlegroundActor("missle", GameActorType.Player, missle);
    }

    private void createZombies() {
        Texture texture = new Texture("player/zombie.png");
        for(int i = 0; i < 10; i++) {
            Image zombie = new Image(texture);
            zombie.setSize(100, 100);
            world.addMiddlegroundActor("zombie_" + i,  GameActorType.Zombie, zombie);
            zombie.setPosition(random.nextFloat()*1000, random.nextFloat()*1000, Align.center);
            zombie.setRotation(random.nextFloat()*360f);
        }
    }
}
