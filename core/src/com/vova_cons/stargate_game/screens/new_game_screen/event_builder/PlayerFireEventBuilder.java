package com.vova_cons.stargate_game.screens.new_game_screen.event_builder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.EventHandler;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_builder.SpecificEventBuilder;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;

/**
 * Created by anbu on 12.06.18.
 */
public class PlayerFireEventBuilder implements SpecificEventBuilder<GameEventType, GameActorType> {
    public static final String ID = "id";
    public static final String START = "start";
    public static final String FINISH = "finish";
    public static final String SPEED = "speed";
    public static final String ANGLE = "angle";
    private float fireDelay = 60f / 800f; //shots per seconds
    private float fireTimeout = 0f;
    private long identifier = 0;

    @Override
    public void update(float delta, World<GameActorType> world, EventHandler<GameEventType> eventHandler) {
        updateFireTimeout(delta);
        if (isCanFire()) {
            if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
                createFire(world, eventHandler);
            }
        }
    }

    private void updateFireTimeout(float delta) {
        if (fireTimeout > 0f) {
            fireTimeout = fireTimeout - delta;
            if (fireTimeout < 0) {
                fireTimeout = 0;
            }
        }
    }

    private boolean isCanFire() {
        return fireTimeout <= 0f;
    }

    private void createFire(World<GameActorType> world, EventHandler<GameEventType> eventHandler) {
        Event<GameEventType> event = new Event<GameEventType>(GameEventType.BulletAdd);
        event.setArg(ID, "bullet_" + identifier);
        identifier++;
        Actor player = world.getActor("player");
        Vector2 seeVector = getSeeVector(world);
        Vector2 startPos = getStartPosition(world);
        event.setArg(START, startPos);
        event.setArg(FINISH, new Vector2(startPos.x + seeVector.x, startPos.y + seeVector.y));
        event.setArg(ANGLE, seeVector.angle());
        event.setArg(SPEED, 2000f);
        eventHandler.handle(event);
        fireTimeout = fireDelay;
    }

    private Vector2 getSeeVector(World<GameActorType> world) {
        Vector2 mousePos = getMouseArrowPosition();
        mousePos = world.stageToLocalCoordinates(mousePos);
        Vector2 playerPos = getStartPosition(world);
        return new Vector2(mousePos.x - playerPos.x, mousePos.y - playerPos.y);
    }

    private Vector2 getStartPosition(World<GameActorType> world) {
        Actor player = world.getActor("player");
        return player.localToParentCoordinates(new Vector2(player.getWidth()*0.90f, player.getHeight()*0.25f));
    }

    private Vector2 getMouseArrowPosition() {
        float x = Gdx.input.getX();
        float y = Gdx.input.getY();
        return new Vector2(x, BaseScreen.HEIGHT-y);
    }
}
