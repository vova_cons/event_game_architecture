package com.vova_cons.stargate_game.screens.new_game_screen.event_builder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.EventHandler;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_builder.SpecificEventBuilder;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;

/**
 * Created by anbu on 12.06.18.
 */
public class PlayerMovingEventBuilder implements SpecificEventBuilder<GameEventType, GameActorType> {
    private float playerSpeed = 200; //pixels per seconds

    @Override
    public void update(float delta, World<GameActorType> world, EventHandler<GameEventType> eventHandler) {
        int dy = 0;
        if (Gdx.input.isKeyPressed(Input.Keys.W)){
            dy = 1;
        } else if (Gdx.input.isKeyPressed(Input.Keys.S)){
            dy = -1;
        }
        int dx = 0;
        if (Gdx.input.isKeyPressed(Input.Keys.D)){
            dx = 1;
        } else if (Gdx.input.isKeyPressed(Input.Keys.A)){
            dx = -1;
        }
        if (dx != 0 || dy != 0){
            Event<GameEventType> event = new Event<GameEventType>(GameEventType.MoveTo);
            event.setArg("id", "player");
            event.setArg("vector", new Vector2(dx * playerSpeed * delta, dy * playerSpeed * delta));
            eventHandler.handle(event);
        }
    }
}
