package com.vova_cons.stargate_game.screens.new_game_screen.event_builder;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.EventHandler;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_builder.SpecificEventBuilder;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;
import com.vova_cons.stargate_game.screens.new_game_screen.model.Bullet;

import java.util.List;

import static com.vova_cons.stargate_game.screens.new_game_screen.event_processor.ActorMovingProcessor.VECTOR;
import static com.vova_cons.stargate_game.screens.new_game_screen.event_processor.ActorMovingProcessor.ID;

/**
 * Created by anbu on 12.06.18.
 */
public class BulletUpdateEventBuilder implements SpecificEventBuilder<GameEventType, GameActorType> {

    @Override
    public void update(float delta, World<GameActorType> world, EventHandler<GameEventType> eventHandler) {
        List<Actor> bullets = world.getTypeActors(GameActorType.Bullet);
        if (bullets != null) {
            for (Actor actor : bullets) {
                if (actor instanceof Bullet) {
                    Bullet bullet = (Bullet) actor;
                    updateBullet(bullet, delta, eventHandler);
                }
            }
        }
    }

    private void updateBullet(Bullet bullet, float delta, EventHandler<GameEventType> eventHandler) {
        float progress = bullet.getProgress();
        if (progress >= 1f){
            Event<GameEventType> event = new Event<GameEventType>(GameEventType.BulletRemove);
            event.setArg("id", bullet.getId());
            eventHandler.handle(event);
            return;
        }
        Vector2 vector = new Vector2(
                bullet.getEndPosition().x - bullet.getBeginPosition().x,
                bullet.getEndPosition().y - bullet.getBeginPosition().y);
        float duration = vector.len() / bullet.getSpeed();
        progress += delta / duration;
        if (progress > 1f){
            progress = 1f;
        }
        bullet.setProgress(progress);
        float curX = bullet.getBeginPosition().x + vector.x * progress;
        float curY = bullet.getBeginPosition().y + vector.y * progress;
        float dx = curX - bullet.getX(Align.center);
        float dy = curY - bullet.getY(Align.center);

        Event<GameEventType> event = new Event<GameEventType>(GameEventType.MoveTo);
        event.setArg(ID, bullet.getId());
        event.setArg(VECTOR, new Vector2(dx, dy));
        eventHandler.handle(event);
    }
}
