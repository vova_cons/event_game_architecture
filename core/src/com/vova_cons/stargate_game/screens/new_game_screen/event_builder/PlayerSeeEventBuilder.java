package com.vova_cons.stargate_game.screens.new_game_screen.event_builder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.EventHandler;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_builder.SpecificEventBuilder;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;

/**
 * Created by anbu on 12.06.18.
 */
public class PlayerSeeEventBuilder implements SpecificEventBuilder<GameEventType, GameActorType> {

    @Override
    public void update(float delta, World<GameActorType> world, EventHandler<GameEventType> eventHandler) {
        Actor player = world.getActor("player");
        Vector2 mousePos = getMouseArrowPosition();
        mousePos = world.stageToLocalCoordinates(mousePos);
        Vector2 playerPos = new Vector2(player.getX() + player.getOriginX(), player.getY() + player.getOriginY());
        Vector2 seeVector = new Vector2(mousePos.x - playerPos.x, mousePos.y - playerPos.y);
        float angle = seeVector.angle();
        Event<GameEventType> event = new Event<GameEventType>(GameEventType.SeeTo);
        event.setArg("id", "player");
        event.setArg("angle", angle);
        eventHandler.handle(event);
    }

    private Vector2 getMouseArrowPosition() {
        float x = Gdx.input.getX();
        float y = Gdx.input.getY();
        return new Vector2(x, BaseScreen.HEIGHT-y);
    }
}
