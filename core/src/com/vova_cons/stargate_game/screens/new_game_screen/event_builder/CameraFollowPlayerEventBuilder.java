package com.vova_cons.stargate_game.screens.new_game_screen.event_builder;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.vova_cons.stargate_game.event_system.Event;
import com.vova_cons.stargate_game.event_system.EventHandler;
import com.vova_cons.stargate_game.event_system.World;
import com.vova_cons.stargate_game.event_system.event_builder.SpecificEventBuilder;
import com.vova_cons.stargate_game.screens.new_game_screen.GameEventType;
import com.vova_cons.stargate_game.screens.new_game_screen.GameActorType;

/**
 * Created by anbu on 12.06.18.
 */
public class CameraFollowPlayerEventBuilder implements SpecificEventBuilder<GameEventType, GameActorType> {
    @Override
    public void update(float delta, World<GameActorType> world, EventHandler<GameEventType> eventHandler) {
        Actor player = world.getActor("player");
        Vector2 cameraPos = new Vector2(
                player.getX() + player.getWidth()/2f,
                player.getY() + player.getHeight()/2f
        );
        Event<GameEventType> event = new Event<GameEventType>(GameEventType.CameraPositionSet);
        event.setArg("position", cameraPos);
        eventHandler.handle(event);
    }
}
