package com.vova_cons.stargate_game.screens.new_game_screen;

/**
 * Created by anbu on 12.06.18.
 */
public enum GameActorType {
    Player, Zombie, Bullet
}
