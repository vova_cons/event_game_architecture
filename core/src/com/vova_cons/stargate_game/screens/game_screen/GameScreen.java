package com.vova_cons.stargate_game.screens.game_screen;

import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.game_screen.game.GameController;
import com.vova_cons.stargate_game.screens.game_screen.game.GameModel;
import com.vova_cons.stargate_game.screens.game_screen.game.GameView;

/**
 * Created by anbu on 20.05.18.
 */
public class GameScreen extends BaseScreen {
    private GameModel model;
    private GameController controller;
    private GameView view;

    @Override
    protected void onStart() {
        model = new GameModel();
        controller = new GameController(model);
        view = new GameView(model, controller);
        this.addActor(view);
    }

    @Override
    protected void onFinish() {

    }

    @Override
    protected void update(float delta) {
        controller.update(delta);
    }
}
