package com.vova_cons.stargate_game.screens.game_screen.player;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.vova_cons.stargate_game.core.Core;
import com.vova_cons.stargate_game.screens.game_screen.game.GameView;
import com.vova_cons.stargate_game.screens.game_screen.level.LevelModel;
import com.vova_cons.stargate_game.services.assets_service.AssetsService;

/**
 * Created by anbu on 20.05.18.
 */
public class PlayerView extends Group {
    private final GameView parent;
    private final PlayerController controller;
    private Image playerImage;

    //region initialization
    public PlayerView(PlayerModel model, PlayerController controller, GameView gameView) {
        this.parent = gameView;
        this.controller = controller;
        controller.setView(this);
        this.setSize(LevelModel.TILE_W, LevelModel.TILE_H);
        createComponents(model);
    }

    private void createComponents(PlayerModel model) {
        AssetsService assetsManager = Core.getInstance().getServicesManager().getService(AssetsService.class);
        Texture texture = assetsManager.get("player");
        playerImage = new Image(texture);
        playerImage.setSize(this.getWidth(), this.getHeight());
        playerImage.setOrigin(Align.center);
        this.addActor(playerImage);
    }
    //endregion

    //region interface
    public void redraw(PlayerModel model){
        playerImage.setRotation(model.getAngle());
        this.setPosition(model.getX(), model.getY(), Align.center);
    }

    public void addBullet(BulletView bulletView) {
        parent.addBullet(bulletView);
    }
    //endregion
}
