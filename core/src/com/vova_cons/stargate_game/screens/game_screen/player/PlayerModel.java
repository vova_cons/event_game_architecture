package com.vova_cons.stargate_game.screens.game_screen.player;

/**
 * Created by anbu on 20.05.18.
 */
public class PlayerModel {
    private float x = 0;
    private float y = 0;
    private float angle = 0;
    private float speed = 150; //pixel per seconds
    private float angleSpeed = 90; //degrees per seconds

    //region getters
    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getAngle() {
        return angle;
    }

    public float getSpeed() {
        return speed;
    }

    public float getAngleSpeed() {
        return angleSpeed;
    }
    //endregion


    //region interface
    public void setAngle(float degress){
        this.angle = degress;
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }
    //endregion

}
