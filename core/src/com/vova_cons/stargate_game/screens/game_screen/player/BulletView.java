package com.vova_cons.stargate_game.screens.game_screen.player;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pool;
import com.vova_cons.stargate_game.core.Core;
import com.vova_cons.stargate_game.services.assets_service.AssetsService;

import java.util.Random;

/**
 * Created by anbu on 05.06.18.
 */
public class BulletView extends Group {
    private static final Random random = new Random(System.currentTimeMillis());
    private final Pool<BulletView> pool;
    private float width = 20f;
    private float speed = 1000f;
    private Vector2 vector;
    private Vector2 startPos;
    private float progress;
    private float duration;

    public BulletView(Pool<BulletView> bulletsPool) {
        this.pool = bulletsPool;
        AssetsService assets = Core.getInstance().getServicesManager().getService(AssetsService.class);
        Texture texture = assets.get("bullet");
        Image image = new Image(texture);
        float scale = width / image.getWidth();
        image.setSize(width, image.getHeight()*scale);
        this.addActor(image);
        this.setSize(image.getWidth(), image.getHeight());
        this.setOrigin(image.getWidth()/2f, image.getHeight()/2f);
    }

    //region interface
    public void reset() {
        this.setVisible(false);
    }

    public void start(Vector2 startPos, Vector2 toPos, float accuracyRadius) {
        this.startPos = startPos;
        this.progress = 0f;
        this.setPosition(startPos.x, startPos.y, Align.center);
        vector = new Vector2(toPos.x - startPos.x, toPos.y - startPos.y);
        calculateAccuracy(accuracyRadius);
        calculateDuration();
        calculateAndApplyAngle();
        this.setVisible(true);
    }

    public void update(float delta){
        if (this.isVisible()) {
            if (progress >= 1f){
                this.setVisible(false);
                pool.free(this);
            }
            progress += delta / duration;
            if (progress > 1f){
                progress = 1f;
            }
            float dx = vector.x * progress;
            float dy = vector.y * progress;
            this.setPosition(startPos.x + dx, startPos.y + dy, Align.center);
        }
    }
    //endregion


    //region logic
    private void calculateAccuracy(float accuracyRadius) {
        float angle = 360f * random.nextFloat();
        float radius = accuracyRadius * random.nextFloat();
        float dx = (float) (Math.sqrt(radius) * Math.cos(angle));
        float dy = (float) (Math.sqrt(radius) * Math.sin(angle));
        vector = vector.add(dx, dy);
    }
    private void calculateDuration(){
        float length = vector.len();
        this.duration = length / speed;
    }

    private void calculateAndApplyAngle() {
        float angle = vector.angle();
        this.setRotation(angle);
    }
    //endregion
}
