package com.vova_cons.stargate_game.screens.game_screen.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.vova_cons.stargate_game.screens.BaseScreen;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by anbu on 20.05.18.
 */
public class PlayerController {
    private final PlayerModel model;
    private PlayerView view;
    private float accuracyStart = 50;
    private float accuracy = accuracyStart;
    private float accuracyIncreaseDelta = 100; //pixels per shot
    private float accuracyDecreaseDelta = 30; //pixels per second
    private float resetShotTime = 0;
    private float shotColdown = 60f / 800f;
    private List<BulletView> bullets = new LinkedList<BulletView>();
    private Pool<BulletView> bulletsPool = new Pool<BulletView>() {
        @Override protected BulletView newObject() {
            BulletView bulletView = new BulletView(bulletsPool);
            view.addBullet(bulletView);
            bullets.add(bulletView);
            return bulletView;
        }
        @Override public void free(BulletView object) {
            object.reset();
            super.free(object);
        }
    };

    //region interface
    public PlayerController(PlayerModel model) {
        this.model = model;
    }

    public void setView(PlayerView view) {
        this.view = view;
    }
    //endregion


    //region interface
    public void update(float delta) {
        if (resetShotTime > 0f){
            resetShotTime = resetShotTime - delta;
        }
        if (accuracy > accuracyStart) {
            accuracy = accuracy - accuracyDecreaseDelta*delta;
            if (accuracy < accuracyStart){
                accuracy = accuracyStart;
            }
        }
        for(BulletView bulletView : bullets){
            bulletView.update(delta);
        }
        updatePosition(delta);
        updateSee(delta);
        updateShots(delta);
        view.redraw(model);
    }
    //endregion


    //region logic
    private void updatePosition(float delta) {
        float dy = 0;
        float dx = 0;
        if (Gdx.input.isKeyPressed(Input.Keys.W)){
            dy += 1f;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)){
            dy -= 1f;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A)){
            dx -= 1f;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)){
            dx += 1f;
        }
        float x = model.getX() + delta * dx * model.getSpeed();
        float y = model.getY() + delta * dy * model.getSpeed();
        model.setPosition(x, y);
    }

    private void updateSee(float delta) {
        Vector2 pos = getMouseArrowPosition();
        float dx = pos.x - model.getX();
        float dy = pos.y - model.getY();
        Vector2 seeVector = new Vector2(dx, dy);
        float angle = seeVector.angle();
        model.setAngle(angle);
    }

    private void updateShots(float delta){
        Vector2 pos = getMouseArrowPosition();
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
            if (resetShotTime <= 0){
                resetShotTime = shotColdown;
                BulletView bullet = bulletsPool.obtain();
                bullet.start(new Vector2(model.getX(), model.getY()), pos, accuracy);
                accuracy = accuracy + accuracyIncreaseDelta;
            }
        }
    }

    private Vector2 getMouseArrowPosition() {
        float x = Gdx.input.getX();
        float y = Gdx.input.getY();
        return new Vector2(x, BaseScreen.HEIGHT-y);
    }
    //endregion
}
