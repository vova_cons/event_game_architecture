package com.vova_cons.stargate_game.screens.game_screen.game;

import com.vova_cons.stargate_game.screens.game_screen.player.PlayerController;

/**
 * Created by anbu on 20.05.18.
 */
public class GameController {
    private GameModel model;
    private GameView view;

    private PlayerController playerController;

    //region initialization
    public GameController(GameModel model) {
        this.model = model;
        playerController = new PlayerController(model.getPlayer());
    }

    public void setView(GameView view) {
        this.view = view;
    }
    //endregion


    //region interface
    public PlayerController getPlayerController() {
        return playerController;
    }

    public void update(float delta){
        playerController.update(delta);
    }
    //endregion
}
