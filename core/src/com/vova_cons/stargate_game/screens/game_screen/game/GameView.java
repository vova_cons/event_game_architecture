package com.vova_cons.stargate_game.screens.game_screen.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.vova_cons.stargate_game.core.Core;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.screens.game_screen.player.BulletView;
import com.vova_cons.stargate_game.screens.game_screen.player.PlayerView;
import com.vova_cons.stargate_game.services.assets_service.AssetsService;

/**
 * Created by anbu on 20.05.18.
 */
public class GameView extends Group {
    private GameController controller;

    public GameView(GameModel model, GameController controller) {
        this.controller = controller;
        controller.setView(this);
        createComponents(model);
    }

    private void createComponents(GameModel model) {
        AssetsService assets = Core.getInstance().getServicesManager().getService(AssetsService.class);
        Texture texture = assets.get("bg");
        Image bg = new Image(texture);
        bg.setSize(BaseScreen.WIDTH, BaseScreen.HEIGHT);
        this.addActor(bg);

        PlayerView playerView = new PlayerView(model.getPlayer(), controller.getPlayerController(), this);
        this.addActor(playerView);
        playerView.setPosition(BaseScreen.WIDTH/2f, BaseScreen.HEIGHT/2, Align.center);
        model.getPlayer().setPosition(playerView.getX() + playerView.getWidth()/2f, playerView.getY() + playerView.getHeight()/2f);
    }

    public void addBullet(BulletView bulletView) {
        this.addActor(bulletView);
    }
}
