package com.vova_cons.stargate_game.screens.game_screen.game;

import com.vova_cons.stargate_game.screens.game_screen.player.PlayerModel;

/**
 * Created by anbu on 20.05.18.
 */
public class GameModel {
    private PlayerModel player;

    public GameModel() {
        player = new PlayerModel();
    }

    public PlayerModel getPlayer() {
        return player;
    }
}
