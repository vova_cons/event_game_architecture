package com.vova_cons.stargate_game.services.assets_service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;
import com.vova_cons.stargate_game.screens.BaseScreen;

/**
 * Created by anbu on 20.05.18.
 */
public class AssetsXmlParser {
    private static final String TAG = "AsetsXmlParser";
    private AssetsService service;

    public static AssetsService parse(FileHandle file) {
        return new AssetsXmlParser().run(file);
    }

    private AssetsService run(FileHandle file) {
        XmlReader reader = new XmlReader();
        XmlReader.Element root = reader.parse(file);
        parseRoot(root);
        return service;
    }

    private void parseRoot(XmlReader.Element root) {
        service = new AssetsService();
        for(XmlReader.Element screenElement : root.getChildrenByName("screen")){
            parseScreen(screenElement);
        }
    }

    private void parseScreen(XmlReader.Element screenElement) {
        String classString = screenElement.getAttribute("screen");
        try {
            Class<? extends BaseScreen> screenClass = (Class<? extends BaseScreen>) Class.forName(classString);
            for(XmlReader.Element assetElement : screenElement.getChildrenByName("asset")){
                parseAsset(screenClass, assetElement);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void parseAsset(Class<? extends BaseScreen> screenClass, XmlReader.Element assetElement) {
        String id = assetElement.getAttribute("id");
        String typeString = assetElement.getAttribute("type");
        String file = assetElement.getAttribute("file");
        try {
            Class<?> typeClass = Class.forName(typeString);
            Asset asset = new Asset(id, file, typeClass);
            service.add(screenClass, asset);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Gdx.app.error(TAG, "error with parse screen=" + screenClass.getSimpleName() +
                    " asset id=" + assetElement.getAttribute("id"));
        }
    }
}
