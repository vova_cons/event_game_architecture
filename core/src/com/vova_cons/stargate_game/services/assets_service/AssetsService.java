package com.vova_cons.stargate_game.services.assets_service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;
import com.vova_cons.stargate_game.screens.BaseScreen;
import com.vova_cons.stargate_game.services.Service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by anbu on 05.05.18.
 */
public class AssetsService implements Service {
    public static final String TAG = "AssetsService";
    private Map<Class<? extends BaseScreen>, List<Asset>> screensMap = new HashMap<Class<? extends BaseScreen>, List<Asset>>();
    private Map<String, AssetFileHandle> assetsMap = new HashMap<String, AssetFileHandle>();

    @Override
    public void onStart() {}

    @Override
    public void update(float delta) {

    }

    @Override
    public void onFinish() {
        for(Class<? extends BaseScreen> screen : screensMap.keySet()){
            unload(screen);
        }
    }


    //region interface
    public <T> T get(String id){
        AssetFileHandle asset = assetsMap.get(id);
        if (asset == null) {
            Gdx.app.error(TAG, "not found asset " + id);
            return null;
        }
        return (T) asset.get();
    }

    public <T> void add(Class<? extends BaseScreen> screen, Asset asset){
        if (!screensMap.containsKey(screen)) {
            screensMap.put(screen, new LinkedList<Asset>());
        }
        List<Asset> assets = screensMap.get(screen);
        assets.add(asset);
    }

    public void load(Class<? extends BaseScreen> type) {
        List<Asset> screen = screensMap.get(type);
        if (screen != null) {
            for(Asset asset : screen){
                String file = asset.getFile();
                String id = asset.getId();
                if (assetsMap.containsKey(id)){
                    AssetFileHandle assetFile = assetsMap.get(id);
                    assetFile.increment();
                } else {
                    assetsMap.put(id, new AssetFileHandle(file, asset.getTClass()));
                }
            }
        }
    }

    public void unload(Class<? extends BaseScreen> type) {
        List<Asset> screen = screensMap.get(type);
        if (screen != null) {
            for(Asset asset : screen){
                String id = asset.getFile();
                if (assetsMap.containsKey(id)){
                    AssetFileHandle assetFile = assetsMap.get(id);
                    assetFile.unload();
                    assetFile.decrement();
                    if (!assetFile.isUsed()){
                        assetsMap.remove(id);
                    }
                }
            }
        }
    }
    //endregion
}
