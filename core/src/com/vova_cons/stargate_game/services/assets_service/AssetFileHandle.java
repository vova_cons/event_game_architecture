package com.vova_cons.stargate_game.services.assets_service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by anbu on 20.05.18.
 */
public class AssetFileHandle {
    private int referenceCount = 0;
    private Object fileHandle;

    public AssetFileHandle(String path, Class<?> type) {
        load(path, type);
    }

    //region interface
    public void increment() {
        referenceCount++;
    }
    public void decrement() {
        referenceCount--;
    }

    public boolean isUsed() {
        return referenceCount > 0;
    }

    public <T> T get() {
        return (T)fileHandle;
    }

    public void load(String path, Class<?> type) {
        if (type == Texture.class){
            fileHandle = new Texture(Gdx.files.internal(path));
        } else {
            Gdx.app.error(AssetsService.TAG, "unknown asset type: " + type.getName());
        }
    }

    public void unload() {
        if (fileHandle != null) {
            if (fileHandle instanceof Disposable){
                ((Disposable) fileHandle).dispose();
            }
            fileHandle = null;
        }
    }
    //endregion
}
