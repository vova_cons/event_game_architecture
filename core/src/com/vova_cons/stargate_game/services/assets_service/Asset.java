package com.vova_cons.stargate_game.services.assets_service;

/**
 * Created by anbu on 20.05.18.
 */
public class Asset {
    private String id;
    private String file;
    private Class<?> tClass;

    public Asset(String id, String file, Class<?> tClass) {
        this.id = id;
        this.file = file;
        this.tClass = tClass;
    }

    public String getId() {
        return id;
    }

    public String getFile() {
        return file;
    }

    public Class<?> getTClass() {
        return tClass;
    }
}
