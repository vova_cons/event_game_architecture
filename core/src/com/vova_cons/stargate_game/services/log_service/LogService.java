package com.vova_cons.stargate_game.services.log_service;

import com.badlogic.gdx.Gdx;
import com.vova_cons.stargate_game.services.Service;

/**
 * Created by anbu on 25.04.18.
 */
public class LogService implements Service {
    private float uptime;

    @Override
    public void onStart() {
        uptime = 0f;
        Gdx.app.log(getTimestamp(), "started game");
    }

    @Override
    public void update(float delta) {
        uptime += delta;
    }

    @Override
    public void onFinish() {
        Gdx.app.log(getTimestamp(), "finished game");
    }

    public void log(String text) {
        Gdx.app.log(getTimestamp(), text);
    }

    private String getTimestamp() {
        int timeInSec = (int) uptime;
        int hours = (timeInSec / 3600);
        int minutes = (timeInSec / 60) % 60;
        int seconds = timeInSec % 60;
        return "[" + twoNum(hours) + ":" + twoNum(minutes) + ":" + twoNum(seconds) + "]";
    }
    private String twoNum(int num){
        if (num < 10){
            return "0" + num;
        }
        return "" + num;
    }
}
