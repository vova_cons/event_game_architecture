package com.vova_cons.stargate_game.services;

/**
 * Created by anbu on 25.04.18.
 */
public interface Service {
    void onStart();
    void update(float delta);
    void onFinish();
}
