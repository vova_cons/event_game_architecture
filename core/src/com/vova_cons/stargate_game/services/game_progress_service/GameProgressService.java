package com.vova_cons.stargate_game.services.game_progress_service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.vova_cons.stargate_game.services.Service;

/**
 * Created by anbu on 06.05.18.
 */
public class GameProgressService implements Service {
    public static final int CLOSED = 0;
    public static final int OPENED = 1;
    public static final int DONE = 2;
    private static final String PREFS_NAME = "com.vova_cons.bool_logic_teach.game_progress.prefs";
    private Preferences prefs;

    @Override
    public void onStart() {
        prefs = Gdx.app.getPreferences(PREFS_NAME);
        String startLevel = "1";
        if (getLevelState(startLevel) == CLOSED){
            openedLevel(startLevel);
        }
    }

    @Override
    public void update(float delta) {}

    @Override
    public void onFinish() {
        prefs.flush();
    }

    public int getLevelState(String id){
        return prefs.getInteger(id, CLOSED);
    }

    public void doneLevel(String id) {
        int nextLevel = Integer.parseInt(id) + 1;
        prefs.putInteger(id, DONE);
        if (getLevelState(""+nextLevel) != DONE) {
            prefs.putInteger("" + nextLevel, OPENED);
        }
        prefs.flush();
    }

    public void openedLevel(String id){
        prefs.putInteger(id, OPENED);
        prefs.flush();
    }
}
