package com.vova_cons.stargate_game.services.font_service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.vova_cons.stargate_game.services.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anbu on 14.01.18.
 */
public class FontService implements Service {
    public enum Size {
        Maximum(150), Big(100), H1(50), H2(40), H3(30), P(20), Small(15);
        public int size;
        Size(int size){
            this.size = size;
        }
    }
    private static final String CHARACTERS =
            "qwertyuiop[]asdfghjkl;'\\zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"|ZXCVBNM<>?" +
            "1234567890-=!@#$%^&*()_+" +
            "йцукенгшщзхъфывапролджэячсмитьбю./ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,!№?`~ёЁ" +
                    "∧∨";
    private Map<Size, BitmapFont> regularFontMap = new HashMap<Size, BitmapFont>();

    //region interface
    @Override
    public void onStart() {
        FileHandle fileHandle = Gdx.files.internal("font/OpenSans-Regular.ttf");
        if (fileHandle.exists()) {
            generateFont(fileHandle, regularFontMap);
        }
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void onFinish() {

    }
    public BitmapFont getFont(Size size){
        return regularFontMap.get(size);
    }
    //endregion


    //region logic
    private void generateFont(FileHandle fileHandle, Map<Size, BitmapFont> fontMap){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fileHandle);
        for(Size size : Size.values()) {
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = size.size;
            parameter.characters = CHARACTERS;
            BitmapFont font = generator.generateFont(parameter);
            fontMap.put(size, font);
        }
        generator.dispose();
    }
    //endregion
}
